function server_url() {
 var url = "https://mobileapp.thestartupsupport.com/ca-app/";
 //   var url = "http://localhost/ca-app/";
    return url;
}



// Validate Email id
function validateEmail(email) {
        var re =/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }


var app = angular.module('myApp', ["ngRoute"]);
app.directive('datepickerPopup', function (){
    return {
        restrict: 'EAC',
        require: 'ngModel',
        link: function(scope, element, attr, controller) {
      //remove the default formatter from the input directive to prevent conflict
      controller.$formatters.shift();
  }
}
});
app.controller('myCtrl', function ($scope,$rootScope, $http, $routeParams, $window, $filter,$timeout) {

    /*window.localStorage.removeItem('userid');
    window.localStorage.removeItem('company_profile_id');
    window.localStorage.removeItem('company_name');*/

    // Monitor length of reminder message
    var firedOnce = false;
    $scope.MonitorLength = function(reminderLength){
        
        if(reminderLength > 160 && firedOnce == false)
        {
            alert("Your Message Character Limit for SMS has exceeded");
                
            firedOnce = true;

         }

         
     }


    // Register Company User For application use
    $scope.register = function (company_name, email_id, mobile_number, password) {

        var formData = {
            "company_name": company_name,
            "email_id": email_id,
            "mobile_number": mobile_number,
            "password": password,
            "appUser" : "Free"
        };

        // Validations 
        if(!validateEmail(email_id))
        {
            alert("Please enter a valid Email id");
            return false;
        }
        if(company_name == undefined || company_name == '')
        {
            alert("Please enter a Name");
            return false;
        }
        if(mobile_number == undefined || mobile_number == '')
        {
            alert("Please enter a valid Mobile number");
            return false;
        }

        if(password == undefined || password == '')
        {
            alert("Please enter a valid Password");
            return false;
        }

        var url = server_url() + "add_company_profile";
        $rootScope.isLoading = true;
        //alert(url);
        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
                 $rootScope.isLoading = false;
                if (response.data.appresponse == 1) {
                    alert("Thank you for registering on Reminder Manager! Please check your Email for details");
                    $window.location = '#!/login';

                } 
                else{

                    alert("This User is already registered");
                    $window.location = '#!/login';
                }
            }, function (response) {
                console.log(response);
            });

    }

    // user login
    $scope.UserLogin = function (uname, pwd) {

        var params = {
            uname: uname,
            password: pwd

        };
//alert(uname + ' '+ pwd);
        /* Validate username and Password */

        if(!validateEmail(uname))
        {
            alert("Please enter a valid Email id");
            return false;
        }
        if (uname == undefined || uname == '') {
            alert("Please enter Username");
            return false;
        }
        if (pwd == undefined || pwd == '') {
            alert("Please enter Password");
            return false;
        }
       
        $rootScope.isLoading = true;
        $http.post(server_url() + "app_user_login", JSON.stringify(params))
            .then(function (response) {
                //console.log(JSON.stringify(params));
               // alert(uname +'  ' +pwd);
               //console.log(response.data);
                $rootScope.isLoading = false;
               if (response.data.authentication == 1) {
                   // alert(response.data.company_name);
                    $window.location = '#!/home';
                    window.localStorage.setItem('userid', response.data.uid);
                    window.localStorage.setItem('company_profile_id', response.data.company_profile_id);
                    window.localStorage.setItem('company_name', response.data.company_name);
                    window.localStorage.setItem('email_id', response.data.email_id);
                    $window.location.reload();

                }
                else {

                    alert("Please enter Valid Username and Password");
                } 

            }, function (response) {
                //Second function handles error
                alert("Something went wrong!");
            });

    }

     /* Global variables */

     $rootScope.isLoading = false;
     $scope.userid = window.localStorage.getItem('userid');
     $scope.company_profile_id = window.localStorage.getItem('company_profile_id');
     $scope.company_name = window.localStorage.getItem('company_name');
     $scope.$timeout = $timeout;
     $scope.isDisabled = false;
     
    // console.log($scope.userid);
     
      /* User logout */

      $scope.UserLogout = function() {

         //alert("1");

         var r = confirm("Are you sure you want to Log out?");
         if (r == true) {
         $rootScope.isLoading = true;



          $http.post(server_url() + "/app_user_logout",{usrid:0})
              .then(function(response) {
                console.log(response.data.appresponse);
               // alert(response.data.appresponse);
               // console.log(response);
                 $rootScope.isLoading = false;
                  window.localStorage.removeItem("company_profile_id");
                  window.localStorage.removeItem("company_name");
                  window.localStorage.removeItem("userid");
                  window.localStorage.removeItem("email_id");
                 
                  $window.location = '#!/login';
                  $window.location.reload();
                
              }, function(response) {
                  //Second function handles error
                  console.log(response);
                  alert("Failed To Logout, Please check your internet connection ")
              }); 

            }
  
          }

    // Get Information Of Company User
    $scope.getcompanyInfo = function () {
        //$scope.company_profileid = $routeParams.CompProf_ID ? $routeParams.CompProf_ID :  $scope.company_profile_id;


        var url = server_url() + "get_company_info";
        //alert(url);
        var formData = {
            company_profile_id: $scope.company_profile_id
        };

        //console.log(JSON.stringify(formData));
        $rootScope.isLoading = true;
        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
             $rootScope.isLoading = false;
             //console.log(response);
               // $scope.company_profile_id = response.data[0].company_profile_id;
                $scope.uid = response.data[0].uid;
                $scope.company_name = response.data[0].company_name;
                $scope.email_id = response.data[0].email_id;
                $scope.mobile_number = parseInt(response.data[0].mobile_number);
                $scope.sender_email_id = response.data[0].sender_email_id;
                $scope.sender_mobile_number = parseInt(response.data[0].sender_mobile_number);
                $scope.greetings_msg = response.data[0].greetings_msg;
                var email_sign = response.data[0].sender_email_signature;
                $scope.sender_email_signature = email_sign.split("<br>").join("\n");

                //console.log($scope.sender_mobile_number);
                // alert($scope.sender_mobile_number);
            }, function (response) {
                console.log(response);
            });
    }

    // Update Company User profile
    $scope.update_profile = function (company_name, email_id, mobile_number, sender_email_id, sender_email_signature,greetings_msg) {
    // alert($scope.company_profile_id);

        var lines = sender_email_signature.split("\n");
        var email_signature = lines.join("<br>");

       // console.log(JSON.stringify(email_signature));
        var formData = {
            "company_name": company_name,
            "email_id": email_id,
            "mobile_number": mobile_number,
            "sender_email_id": sender_email_id,
            "greetings_msg": greetings_msg,
            "sender_email_signature": email_signature,
            "company_profile_id":  $scope.company_profile_id,
            "uid": $scope.userid
            //   "sender_mobile_number": sender_mobile_number,
        };

        $rootScope.isLoading = true;
         var url = server_url() + "update_company_profile";

        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
             $rootScope.isLoading = false;
               // console.log(response);
                alert(response.data.appresponse);
                $window.location = '#!/home';
                $window.location.reload();
            }, function (response) {
                alert("No Change found");
               // console.log(response);
            });

    }


    $scope.addReceiver = function (receiver_name, is_active, priamry_name, primary_email, primary_mobile, secondary_name, secondary_email, secondary_mobile, third_name, third_email, third_mobile) {
        


        var formData = {
            "receiver_name": receiver_name,
            "is_active": is_active,
            "company_profile_id": $scope.company_profile_id,
            "contacts": [{
                "name": priamry_name,
                "email": primary_email,
                "mobile": primary_mobile,
                "contacttype": 1
            },
            {
                "name": secondary_name,
                "email": secondary_email,
                "mobile": secondary_mobile,
                "contacttype": 2
            },
            {
                "name": third_name,
                "email": third_email,
                "mobile": third_mobile,
                "contacttype": 3
            }
            ]
        };

        var url = server_url() + "add_receiver";
      
       // console.log(JSON.stringify(formData));
        //alert(url);
         $rootScope.isLoading = true;
        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
             $rootScope.isLoading = false;
               //  console.log(url);
                alert(response.data.appresponse);
                $window.location = "#!/receiver_management";
            }, function (response) {
                alert("Failed to Add Receiver");
                 $window.location = "#!/receiver_management";
               // console.log(response);
            });
    }

    // Get Information Of Company User
    $scope.receiverList = [];
    $scope.getAllReceivers = function () {
        //$scope.companyProfileId = $scope.company_profile_id;
        $scope.isDisabled = false;
        $scope.selectall_receivers = false;
        $scope.selected_receivers = [];
        $scope.reminderId = $routeParams.reminder_id;
        var formData = {
            company_profile_id: $scope.company_profile_id,
            reminder_id : $scope.reminderId
        }
        //alert($scope.company_profile_id);
         $rootScope.isLoading = true;
        var url = server_url() + "get_all_receivers";

        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
                // console.log(response.data);
                  $rootScope.isLoading = false;

                  
                  $scope.receiverList = response.data;
                  
                    

            }, function (response) {
                console.log(response);
            });
    }

    // Get Information Of Company User
    $scope.getReceiverInfo = function () {
        $scope.receiver_id = $routeParams.Receiver_ID;
        //alert($scope.receiver_id);
        var formData = {
            receiver_id: $scope.receiver_id
        }

        var url = server_url() + "get_receiver_info";
        $rootScope.isLoading = true;

        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
                 // alert(response.data.is_active);
                 $rootScope.isLoading = false;
                $scope.receiver_name = response.data.receiver_name;
                $scope.is_active = response.data.is_active;
                $scope.company_profile_id = response.data.company_profile_id;

                $scope.receiverContactInfo = response.data.contacts;
               // var arr = [];

              /*  for(var i = 0 ; i <= (3 - response.data.contacts.length); i++)
                {
                    //if($scope.receiverContactInfo[i].contacttype != i)
                            $scope.max = Math.max.apply(Math,$scope.receiverContactInfo.map(function(item){return item.contacttype;}));

                            console.log($scope.max);

                                var param = {"name":"","email_id":"","mobile_number":"","contacttype": $scope.max+1};
                             $scope.receiverContactInfo.push(param);
                         
                                
                           
                    }*/
                
               // console.log(response.data.contacts.length);
              //  console.log(JSON.stringify($scope.receiverContactInfo));
            }, function (response) {
                console.log(response);
            });
    }


    //Get inactive receivers
    $scope.getInactiveReceivers = function () {
        //$scope.companyProfileId = $scope.company_profile_id;

        var formData = {
            company_profile_id: $scope.company_profile_id
        }
        //alert($scope.company_profile_id);
         $rootScope.isLoading = true;
        var url = server_url() + "get_inactive_receivers";

        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
                 //console.log(response.data);
                  $rootScope.isLoading = false;
                  $scope.inactivereceiverList = response.data;

            }, function (response) {
                //console.log(response);
            });
    }


    // Unset variables to blank
    $scope.UnsetRecvrVariables = function(){

        $scope.receiver_name = "";
        $scope.is_active = "";
        $scope.priamry_name = "";
        $scope.primary_email = "";
        $scope.primary_mobile = "";
        $scope.secondary_name = "";
        $scope.secondary_email = "";
        $scope.secondary_mobile = "";
        $scope.third_name = "";
        $scope.third_email = "";
        $scope.third_mobile = "";
    }

    $scope.updateReceiver = function (receiver_name, is_active, receiverContactInfo) {
        $scope.receiver_id = $routeParams.Receiver_ID;
        var formData = {
            "receiver_name": receiver_name,
            "is_active": is_active,
            "company_profile_id": $scope.company_profile_id,
            "contacts": receiverContactInfo,
            "receiver_id": $scope.receiver_id
        };
        $rootScope.isLoading = true;
        var url = server_url() + "update_receiver";
       // console.log(JSON.stringify(formData));
        //alert(url);
        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
             $rootScope.isLoading = false;
              //  console.log(response.data);
                $window.location = "#!/receiver_management";
                alert(response.data.appresponse);
            }, function (response) {
                alert("Failed to Add Receiver");
                 $window.location = "#!/receiver_management";
                //console.log(response);
            });
    }

    // Get all reminder
     $scope.reminderList = [];
    $scope.getAllReminders = function () {
        $scope.selectall = false;
        $scope.reminder_ids = [];
        $scope.receiver_id = $routeParams.receiver_id;
        //alert($scope.company_profile_id);
        var formData = {
            "company_profile_id": $scope.company_profile_id,
            "receiver_id" : $scope.receiver_id
        }

        var url = server_url() + "get_all_reminders";
      //  console.log(JSON.stringify(formData));
        $rootScope.isLoading = true;
        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
                $rootScope.isLoading = false;

              //  console.log(response.data);
                $scope.reminderList = response.data;
            }, function (response) {
               // console.log(response);
            });
    }

    // Get Information Of Company User
    $scope.getReminderInfo = function () {
        $scope.reminder_id = $routeParams.Reminder_ID;
        //alert($scope.receiver_id);
        var formData = {
            reminder_id: $scope.reminder_id
        }

        var url = server_url() + "get_reminder_info";
        $rootScope.isLoading = true;

        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
                $rootScope.isLoading = false;
                $scope.reminder_name = response.data[0].reminder_name;
                //alert($scope.reminder_name);
                $scope.reminder_text = response.data[0].reminder_text;
                // alert($scope.reminder_text);
                 // $scope.due_date =  $filter('date')(response.data[0].due_date, "medium", 'UTC');
                 $scope.due_date =  response.data[0].due_date;
                //alert($scope.due_date);
                $scope.repetitive_y_n = response.data[0].repetitive_y_n;
                $scope.email_frequency = response.data[0].email_frequency;
                $scope.sms_frequency = response.data[0].sms_frequency;
                $scope.no_of_days_before = parseInt(response.data[0].no_of_days_before);
                $scope.reminder_frequency = response.data[0].reminder_frequency_idreminder_frequency;
                // $scope.sms_frequency = response.data[0].sms_frequency;

                //console.log(response.data.contacts);
            }, function (response) {
               // console.log(response);
            });
    }

    $scope.addReminder = function (reminder_name, reminder_text, due_date, repetitive_y_n, email_frequency, sms_frequency, no_of_days_before, reminder_frequency) {
        //alert(due_date);
        var dateAsString = $filter('date')(due_date, "yyyy-MM-dd");
        $scope.company_profile_id = $scope.company_profile_id;

      
        
        // console.log(dateAsString);
        var formData = {
            "reminder_name": reminder_name,
            "reminder_text": reminder_text,
            "due_date": dateAsString,
            "repetitive_y_n": repetitive_y_n,
            "email_frequency": email_frequency,
            "sms_frequency": sms_frequency,
            "no_of_days_before": no_of_days_before,
            "reminder_frequency": reminder_frequency,
            "company_profile_id": $scope.company_profile_id
        }
        var url = server_url() + "add_reminder";
       // console.log(JSON.stringify(formData));
        $rootScope.isLoading = true;
        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
             $rootScope.isLoading = false;
            // console.log(response.data);
               // console.log(response.data.appresponse);
                alert(response.data.appresponse);
                $window.location = "#!/reminder_management";
            }, function (response) {
                alert("Failed to Add Reminder");
                $window.location = "#!/reminder_management";
               // console.log(response.data.appresponse);
            });
    }

    $scope.updateReminder = function (reminder_name, reminder_text, due_date, repetitive_y_n, email_frequency, sms_frequency, no_of_days_before, reminder_frequency) {
        $scope.reminder_id = $routeParams.Reminder_ID;

        var dateAsString = $filter('date')(due_date, "yyyy-MM-dd");
        //$scope.company_profile_id = $scope.company_profile_id;
        // console.log(dateAsString);
        var formData = {
            "reminder_name": reminder_name,
            "reminder_text": reminder_text,
            "due_date":dateAsString,
            "repetitive_y_n": repetitive_y_n,
            "email_frequency": email_frequency,
            "sms_frequency": sms_frequency,
            "no_of_days_before": no_of_days_before,
            "reminder_frequency": reminder_frequency,
            "company_profile_id": $scope.company_profile_id,
            "idreminder": $scope.reminder_id
        }
        var url = server_url() + "update_reminder";
       // console.log(JSON.stringify(formData));

        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
               // console.log(response.data);
               // alert(response.data);
                alert(response.data.appresponse);
                $window.location = "#!/reminder_management";
            }, function (response) {
                alert("Failed to Update Reminder");
                $window.location = "#!/reminder_management";
                //console.log(response);
            });
    }

    $scope.get_reminder_frequency = function () {
         // Unset the variables to blank
        $scope.reminder_name = "";
        $scope.reminder_text = ""; 
        $scope.due_date = "";
        $scope.repetitive_y_n = "y";
        $scope.email_frequency = "";
        $scope.sms_frequency = "";
        $scope.no_of_days_before = "";
        $scope.reminder_frequency = "";
        var url = server_url() + "get_reminder_frequency";
        $http.get(url).then(function (response) {
            $scope.reminder_frq = response.data;

        }, function (response) {
            alert("Failed To load");
        });
    }


    // selected checkboxes of receiver
    $scope.receiver_idreceiver_contacts = [];
    $scope.$watchCollection('receiver_idreceiver_contacts', function (newVal) {
        for (var i = 0; i < newVal.length; ++i) {
            console.log(newVal[i]);
        }
    });


   // select all checkboxes for receivers
    $scope.checkall = false;
    $scope.toggleAll = function() {
        
    $scope.checkall = !$scope.checkall;
   //console.log("fn click" +$scope.checkall );
    $scope.receiver_idreceiver_contacts = [];
    for(var key in $scope.contactList) {
        var contact_ids = $scope.contactList[key].idreceiver_contacts;
        var already_assigned = $scope.contactList[key].already_assign;
       
        if($scope.checkall == true && already_assigned == 'no')
        {  
            $scope.receiver_idreceiver_contacts.push({value: contact_ids});
        }
        else{
           
             $scope.receiver_idreceiver_contacts.push({value: undefined});
        }
      // console.log(JSON.stringify($scope.receiver_idreceiver_contacts));
    }
  };



  

    $scope.assign_reminderToreceiver = function () {

       $scope.receiver_id = $routeParams.receiver_id;

        var reminderid_arr = [];
       for(var key in $scope.reminder_ids)
       {

        reminderid_arr.push($scope.reminder_ids[key].value);
       }
        //alert($scope.receiver_id);
        var formData = {
            "receiver_id": $scope.receiver_id,
            "reminder_ids": reminderid_arr
        };
        var url = server_url() + "assign_reminderToreceiver";
        $rootScope.isLoading = true;
        console.log(JSON.stringify(formData));
        $http.post(url, JSON.stringify(formData))
            .then(function (response) {

                $rootScope.isLoading = false;
               // console.log(response.data);
                alert(response.data.appresponse)
                
                $window.location = "#!/receiver_management";
            }, function (response) {
               // console.log(response);
            });

    }

    // Get all reminder
    $scope.contactList = [];
    $scope.getReceiverContacts = function () {
        $scope.receiver_idreceiver_contacts = [];
        $scope.checkall = false;
        $scope.companyProfileId = $scope.company_profile_id;
        var recevr_id = $routeParams.Receiver_ID;
        $scope.reminderId = $routeParams.reminder_id;

        var formData = {
            company_profile_id: $scope.companyProfileId,
            receiver_id : recevr_id,
            reminder_id : $scope.reminderId
        }

        var url = server_url() + "get_receiver_contacts";
        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
                $scope.contactList = response.data;
            }, function (response) {
                console.log(response);
            });
    }

    // selected checkboxes of reminder services
    $scope.reminder_ids = [];
    $scope.$watchCollection('reminder_ids', function (newVal) {
        for (var i = 0; i < newVal.length; ++i) {
            console.log(newVal[i]);
        }
    });


    // Select All checkboxes for reminders
  
    $scope.selectall = false;
  $scope.toggleReminders = function() {
        
    $scope.selectall = !$scope.selectall;
   //console.log("fn click" +$scope.checkall );
    $scope.reminder_ids = [];
    for(var key in $scope.reminderList) {
        var reminderId = $scope.reminderList[key].idreminder;
        var already_assigned = $scope.reminderList[key].already_assign;
       
        if($scope.selectall == true && already_assigned == 'no')
        {  
            $scope.reminder_ids.push({value: reminderId});
        }
        else{
           
             $scope.reminder_ids.push({value: undefined});
        }
     
    }
    //console.log(JSON.stringify($scope.reminder_ids));
  };

    // Assign receiver to reminder
    $scope.assign_receiverToreminder = function () {

       $scope.reminder_id = $routeParams.reminder_id;
       var receiver_id = $routeParams.Receiver_ID;
        var receiver_contacts = [];
       for(var key in $scope.receiver_idreceiver_contacts)
       {

        receiver_contacts.push($scope.receiver_idreceiver_contacts[key].value);
       }
        var formData = {
            "reminder_id": $scope.reminder_id,
            "receiver_id":receiver_id,
            "receiver_idreceiver_contacts": receiver_contacts
        };
        var url = server_url() + "assign_receiverToreminder";
        $rootScope.isLoading = true;
        //console.log(JSON.stringify(formData));
        $http.post(url, JSON.stringify(formData))
            .then(function (response) {
               // console.log(response.data);
              //   $scope.receiver_idreceiver_contacts = [];
                $rootScope.isLoading = false;
                alert(response.data.appresponse)
                 $window.location = "#!/reminder_management";
               // console.log(response.data.appresponse);
            }, function (response) {
               // console.log(response.data);
            });
        //alert(receiver_idreceivers[0]);
       
    }


   $scope.show_todays_reminder = function(smsStatus){
        // alert("testtttt");
        var company_profile_id = window.localStorage.getItem('company_profile_id');
        show_reminders_list(company_profile_id,smsStatus);

     }

    $scope.connection_call = function(){
            //alert($scope.company_profile_id);
            $rootScope.isLoading = true;
           connections($scope.company_profile_id);
           $rootScope.isLoading = false;
        }

  $scope.email_scheduler = function(){
          var formData = {
              company_profile_id: $scope.company_profile_id
          }
           var url = server_url() + "email_schedule";
           $rootScope.isLoading = true;
           //alert(url);
           $http.post(url, formData)
               .then(function (response) {
                $rootScope.isLoading = false;
                   alert(response.data.email_status)
                  // console.log(response.data.email_status);
               }, function (response) {
                   //console.log(response);
             });
        }

    // Get Completion Dates
    $scope.completion_date_list = [];
    $scope.getCompletionDates = function(){
        $scope.isDisabled = false;
       $scope.completion_date_arr = [];
        $scope.reminderId = $routeParams.reminder_id;
        $scope.receiverId = $routeParams.receiver_id;
        var formData = {
              company_profile_id: $scope.company_profile_id,
              reminder_id : $scope.reminderId,
              receiver_id : $scope.receiverId
          }
           var url = server_url() + "manage_completion_dates";
           $rootScope.isLoading = true;
           //alert(url);
           $http.post(url, JSON.stringify(formData))
               .then(function (response) {
                $rootScope.isLoading = false;
                  // alert(response.data.email_status)
                  $scope.completion_date_list = response.data;
                //   console.log(response.data);
               }, function (response) {
                  // console.log(response);
             });

    }


    // Reminder list for Completion
    $scope.getReminderForCompletion = function(){
        var formData = {company_profile_id : $scope.company_profile_id};
        var url = server_url() + "reminder_for_completion";
       // console.log(JSON.stringify(formData));
           $rootScope.isLoading = true;
          // alert(url);
           $http.post(url, JSON.stringify(formData))
               .then(function (response) {
                $rootScope.isLoading = false;
                  
                  $scope.reminder_completion_list = response.data;
                  // console.log(response.data);
               }, function (response) {
                  // console.log(response);
             });

    }

     // Receiver list for Completion
    $scope.getReceiverForCompletion = function(){

       // console.log("clicked");
        var formData = {company_profile_id : $scope.company_profile_id};
        var url = server_url() + "receiver_for_completion";

        //console.log(JSON.stringify(formData));
           $rootScope.isLoading = true;
          // alert(url);
           $http.post(url, JSON.stringify(formData))
               .then(function (response) {
                $rootScope.isLoading = false;
                  
                  $scope.receiver_completion_list = response.data;
                  // console.log(response.data);
               }, function (response) {
                 //  console.log(response);
             });

    }


    //retrieve completion date record info

    $scope.getCompDateInfo = function(){
        $scope.recvr_reminder_id = $routeParams.recvr_reminder_id;
        var formData = {company_profile_id : $scope.company_profile_id,
            idreceiver_reminder: $scope.recvr_reminder_id};
        var url = server_url() + "get_completion_date_info";
           $rootScope.isLoading = true;
          // alert(url);
           $http.post(url, JSON.stringify(formData))
               .then(function (response) {
                $rootScope.isLoading = false;
                  // alert(response.data.email_status)
                  $scope.completion_date = response.data.completion_date;
                  // console.log(response.data);
               }, function (response) {
                  // console.log(response);
             });


    }

   

    // Update completion date


    
    $scope.completion_date_arr = [];

     $scope.UpdateCompletionDate = function(){
        //$scope.recvr_reminder_id = $routeParams.recvr_reminder_id;

      
       $scope.isDisabled = true;
       var send_arr = [];
      // console.log(JSON.stringify($scope.completion_date_arr));
       for(i=0; i<$scope.completion_date_arr.length;i++)
       {

           // console.log(" jdhfkjdhfsk "+$scope.completion_date_list[i].idreceiver);

           if($scope.completion_date_arr[i] != null)
           {
            
             var dateAsString = $filter('date')($scope.completion_date_arr[i], "yyyy-MM-dd");
             send_arr.push({'completion_date':dateAsString,'idreceiver':$scope.completion_date_list[i].idreceiver});
          
            }
          
        
       }

     
     //  
        var formData = {company_profile_id : $scope.company_profile_id,
            completionData : send_arr,
            idreminder : $routeParams.reminder_id};
       
            //console.log(JSON.stringify(formData));


        var url = server_url() + "update_completion_date";
           $rootScope.isLoading = true;
          // alert(url);
           $http.post(url, JSON.stringify(formData))
               .then(function (response) {
                $rootScope.isLoading = false;
                   alert(response.data.appresponse);
                 $window.location = '#!/completion_reminder_list';
                 //  console.log(response.data);
               }, function (response) {
                  // console.log(response);
             });


    }


    //Update completion date Receiver wise
    $scope.UpdateCompletionReceiverwise = function(){
        //$scope.recvr_reminder_id = $routeParams.recvr_reminder_id;

      
       $scope.isDisabled = true;
       var send_arr = [];
      // console.log(JSON.stringify($scope.completion_date_arr));
       for(i=0; i<$scope.completion_date_arr.length;i++)
       {

           // console.log(" jdhfkjdhfsk "+$scope.completion_date_list[i].idreceiver);

           if($scope.completion_date_arr[i] != null)
           {
            
             var dateAsString = $filter('date')($scope.completion_date_arr[i], "yyyy-MM-dd");
             send_arr.push({'completion_date':dateAsString,'idreminder':$scope.completion_date_list[i].idreminder});
          
            }
          
        
       }

     
     //  
        var formData = {company_profile_id : $scope.company_profile_id,
            completionData : send_arr,
            idreceiver : $routeParams.receiver_id};
       
            //console.log(JSON.stringify(formData));


        var url = server_url() + "update_completion_date_receiverwise";
           $rootScope.isLoading = true;
          // alert(url);
           $http.post(url, JSON.stringify(formData))
               .then(function (response) {
                $rootScope.isLoading = false;
                   alert(response.data.appresponse);
                 $window.location = '#!/completion_reminder_list';
                 //  console.log(response.data);
               }, function (response) {
                  // console.log(response);
             });


    }

    // Send Feedback
    $scope.SendFeedback = function(feedback_message){

        var emailId = window.localStorage.getItem("email_id");
        var formData = {sender_email: emailId, message : feedback_message};
        var url = server_url() + "send_feedback";

        $rootScope.isLoading = true;

         $http.post(url, JSON.stringify(formData))
               .then(function (response) {
                $rootScope.isLoading = false;
                   alert(response.data.appresponse);
                 $window.location = '#!/';
                 //  console.log(response.data);
               }, function (response) {
                   console.log(response);
             });


    }

    $scope.selected_receivers = [];
    
    $scope.selectall_receivers = false;
    $scope.toggleAllReceivers = function() {
      
    $scope.selectall_receivers = !$scope.selectall_receivers;
  
    $scope.selected_receivers = [];
   
    for(var key in $scope.receiverList) {
        var receiverIds  = $scope.receiverList[key].idreceiver;
        var already_assigned = $scope.receiverList[key].already_assign;
       
        if($scope.selectall_receivers == true && already_assigned == 'no' )
        {  
            $scope.selected_receivers.push(receiverIds);
        }
        else{
           
             $scope.selected_receivers.push(undefined);
        }
      // console.log(JSON.stringify($scope.receiver_idreceiver_contacts));
    }
  };

    $scope.AssignSelectedReceivers = function(){
        $scope.isDisabled = true;
        var reminder_id = $routeParams.reminder_id;
        var formData = {company_profile_id : $scope.company_profile_id,
            assign_receivers : $scope.selected_receivers,
            reminderId : reminder_id};
            $rootScope.isLoading = true;
          var url = server_url() + "assign_selected_receivers";  

        //console.log(JSON.stringify($scope.selected_receivers));
        
         $http.post(url, JSON.stringify(formData))
               .then(function (response) {
                $rootScope.isLoading = false;
                alert(response.data.appresponse);
                 $window.location = '#!/reminder_management';
                 //  console.log(response.data);
               }, function (response) {
                  // console.log(response);
             });

    }

            // Function for notification pop up
            $scope.NotificationPopup = function(){

                var formData = {company_profile_id : $scope.company_profile_id};
                
                var url = server_url() + "notification_count";  

               
                $http.post(url, JSON.stringify(formData))
                .then(function (response) {
               
               // alert(response.data.appresponse);
                $scope.count_sms = response.data.smscnt;
                $scope.count_email = response.data.emailcnt;
               // console.log("SMS "+$scope.count_sms+"--- Email "+$scope.count_email);
                 
                }, function (response) {
                   //console.log(response);
                });

            }

               //Dashboards//////

        $scope.UserDashboard = function(reminder_id,from_date,to_date){
                $scope.dashboard_data = [];
                $scope.varmsg = '';
                $rootScope.isLoading = true;
                 $scope.reminderId = reminder_id;

                  

                var formData = {company_profile_id : $scope.company_profile_id,
                    reminder_id: $scope.reminderId,
                    };
                
                var url = server_url() + "reminderwise_receiver_dashboard";  
               
               console.log(JSON.stringify(formData)+"-----"+from_date);
                $http.post(url, JSON.stringify(formData))
                .then(function (response) {
               $rootScope.isLoading = false;
               // alert(response.data.appresponse);
               //console.log(response.data);
               if(response.data.length != 0)
               {
                $scope.dashboard_data = response.data;
                $scope.from_date = from_date;
                $scope.to_date = to_date;

                } else{
                $scope.varmsg = 'This Reminder is not assigned yet!';
                }
                $window.location = '#!/user_dashboard';
                
               // console.log(JSON.stringify($scope.dashboard_data));
                // console.log(response.data);
                }, function (response) {
                   console.log(response);
                });

            }

             $scope.ReceiverDashboard = function(receiver_id,from_date,to_date){

                $scope.receiver_dashboard = [];
                $scope.msg = '';
                $rootScope.isLoading = true;
                $scope.receiverId = receiver_id;
                 var formatted_from_date =  $filter('date')(from_date, "yyyy-MM-dd");  
                var formatted_to_date =  $filter('date')(to_date, "yyyy-MM-dd");
                var formData = {company_profile_id : $scope.company_profile_id,
                    receiverId : $scope.receiverId};
                
                var url = server_url() + "receiverwise_reminder_dashboard";  
               $scope.dateVar = new Date();
              // console.log(JSON.stringify(formData));
                $http.post(url, JSON.stringify(formData))
                .then(function (response) {
               $rootScope.isLoading = false;
               // alert(response.data.appresponse);
                $window.location = '#!/receiver_dashboard';

                    if(response.data.length != 0)
                    {
                        $scope.receiver_dashboard = response.data;
                       $scope.from_date = from_date;
                        $scope.to_date = to_date;
                    }
                    else{

                        $scope.msg = "No reminders are assigned to this Receiver";
                    }
                
                
               // console.log(JSON.stringify($scope.dashboard_data));
              //   console.log(response.data);
                }, function (response) {
                 //  console.log(response);
                });

            }


            //Password reset
            $scope.PasswordReset = function(pwd,confirmpwd){

                    if(pwd != confirmpwd)
                    {
                        alert("Password and Confirm Password fields do not match!");
                        return false;
                    }

                var formData = {"company_profile_id" : $scope.company_profile_id,
                "uid" : $scope.userid,"password" : pwd};

                $rootScope.isLoading = true;

                var url = server_url() + "password_reset"; 

                 $http.post(url, JSON.stringify(formData))
                .then(function (response) {
                    $rootScope.isLoading = false;
               

                    alert(response.data.appresponse);
                    $window.location = '#!/';
                
               // console.log(JSON.stringify($scope.dashboard_data));
              //   console.log(response.data);
                }, function (response) {
                  // console.log(response);
                });

            }

            // Unassign a receiver
            $scope.UnassignReceiver = function(reminder_id,receiverId,contactid){


              var formData = {"reminder_id" : reminder_id,
              "receiver_id" : receiverId, "company_profile_id": $scope.company_profile_id,
              "contactId" : contactid};

              var url = server_url() + "unassign_receiver_reminder"; 

              console.log(JSON.stringify(formData));
               $http.post(url, JSON.stringify(formData))
                .then(function (response) {
                    $rootScope.isLoading = false;
               

                    alert(response.data.appresponse);
                    $window.location = '#!/reminder_management';
                
               // console.log(JSON.stringify($scope.dashboard_data));
              //   console.log(response.data);
                }, function (response) {
                  // console.log(response);
                });

            }

             // Unassign a receiver
            $scope.UnassignReminder = function(reminder_id,receiverId){
              var formData = {"reminder_id" : reminder_id,
              "receiver_id" : receiverId, "company_profile_id": $scope.company_profile_id};

              var url = server_url() + "unassign_receiver_reminder"; 

             // console.log(JSON.stringify(formData));
               $http.post(url, JSON.stringify(formData))
                .then(function (response) {
                    $rootScope.isLoading = false;
               

                    alert(response.data.appresponse);
                    $window.location = '#!/receiver_management';
                
               // console.log(JSON.stringify($scope.dashboard_data));
              //   console.log(response.data);
                }, function (response) {
                  // console.log(response);
                });

            }


            // Get reminder list as per selected due date

            $scope.getDashboardReminder = function(from_date,to_date){
                $scope.dashboard_reminderData = [];
                $scope.message = "";
                    var formatted_from_date =  $filter('date')(from_date, "yyyy-MM-dd");  
                    var formatted_to_date =  $filter('date')(to_date, "yyyy-MM-dd");

                    var formData = {company_profile_id : $scope.company_profile_id,
                        fromDate : formatted_from_date,
                        toDate: formatted_to_date};
                    

                    //console.log(JSON.stringify(formData));
                    var url = server_url() + "get_dashboard_reminder";  
                    $rootScope.isLoading = true;
                    $http.post(url, JSON.stringify(formData))
                    .then(function (response) {
                        $rootScope.isLoading = false;
                   
                      //  console.log(response.data);
                      //  alert(response.data.appresponse);
                      if(response.data.length != 0)
                      {
                         $scope.dashboard_reminderData = response.data;


                      }
                      else
                      {
                        $scope.message = "There are no reminders for the requested dates!";

                      }
                      $scope.fromDate = $filter('date')(from_date, "dd-MM-yyyy");
                      $scope.toDate = $filter('date')(to_date, "dd-MM-yyyy");
                       
                        $window.location = '#!/dashboard_reminder_list';
                    
                   // console.log(JSON.stringify($scope.dashboard_data));
                  //   console.log(response.data);
                    }, function (response) {
                      // console.log(response);
                    });


            }


             // Get receiver list as per selected due date

            $scope.getDashboardReceiver = function(from_date,to_date){
                    var formatted_from_date =  $filter('date')(from_date, "yyyy-MM-dd");  
                    var formatted_to_date =  $filter('date')(to_date, "yyyy-MM-dd");
                    $scope.dashboard_receiverData = [];
                    $scope.message = "";
                    var formData = {company_profile_id : $scope.company_profile_id,
                        fromDate : formatted_from_date,
                        toDate: formatted_to_date};
                    

                    //console.log(JSON.stringify(formData));
                    var url = server_url() + "get_dashboard_receiver";  

                    $rootScope.isLoading = true;
                    $http.post(url, JSON.stringify(formData))
                    .then(function (response) {
                        $rootScope.isLoading = false;
                   
                      //  console.log(response.data);
                      //  alert(response.data.appresponse);
                      if(response.data.length != 0)
                      {
                         $scope.dashboard_receiverData = response.data;


                      }
                      else
                      {
                        $scope.message = "There are no reminders for the requested dates!";

                      }
                      $scope.fromDate = $filter('date')(from_date, "dd-MM-yyyy");
                      $scope.toDate = $filter('date')(to_date, "dd-MM-yyyy");
                       
                        $window.location = '#!/dashboard_receiver_list';
                    
                   // console.log(JSON.stringify($scope.dashboard_data));
                  //   console.log(response.data);
                    }, function (response) {
                      // console.log(response);
                    });


            }


          /*  $scope.$watch(function(scope) { return scope.priamry_name },
                          function(newValue, oldValue) {
                              console.log("Watcher -- old "+oldValue+" -- New value "+newValue);
                          }
                         );*/

            // Get contact Number

            $scope.BrowseContacts = function(contact_type) {


                        navigator.contacts.pickContact(function(contact){
                            console.log('The following contact has been selected:' + JSON.stringify(contact));
                            $scope.$apply(function(){


                                for(i=0 ; i < contact.phoneNumbers.length ; i++)
                                {
                                var phno = contact.phoneNumbers[i].value;
                                var contactname = contact.displayName;
                               // phno.slice(-3);
                                }
                                var contactno = parseInt(phno.replace(/[^\d]/g, '').slice(-10));

                               if(contact_type == 'primary_mobile' )
                               {

                                          $scope.priamry_name = contactname;

                                           $scope.primary_mobile = contactno;



                               }
                               else if(contact_type == 'secondary_mobile')
                               {
                               $scope.secondary_name = contactname;
                               $scope.secondary_mobile = contactno;

                               }
                                else if(contact_type == 'third_mobile' )
                                {
                                $scope.third_name = contactname;
                                $scope.third_mobile = contactno;
                                }

                                if(typeof contact_type == 'object') {
                                     $scope.contact_type = contact_type;

                                      $scope.contact_type.name = contactname;
                                      $scope.contact_type.mobile_number = contactno;

                                     }


                            });


                        },function(err){
                            console.log('Error: ' + err);
                        });
                    };


            // Get contact Name
                    /*$scope.BrowseNames = function(contact_type) {
                                navigator.contacts.pickContact(function(contact){
                                    console.log('The following contact has been selected:' + JSON.stringify(contact));
                                    $scope.$apply(function(){

                                        var contactname = contact.displayName;
                                       // phno.slice(-3);

                                       // var contactno = phno.slice(-10);

                                       if(contact_type == 'priamry_name')
                                       {
                                       $scope.priamry_name = contactname;
                                       }
                                       else if(contact_type == 'secondary_name')
                                       {
                                       $scope.secondary_name = contactname;
                                       }
                                        else if(contact_type == 'third_name')
                                        {
                                        $scope.third_name = contactname;
                                        }

                                    });
                                },function(err){
                                    console.log('Error: ' + err);
                                });
                            };*/


                        $scope.getBlankCompletionCount = function() {
                            //$scope.isDisabled = false;
                           // $scope.completion_date_arr = [];
                            $scope.reminderId = $routeParams.reminder_id;
                            $scope.receiverId = $routeParams.receiver_id;
                            var formData = {
                            company_profile_id: $scope.company_profile_id,
                            reminder_id : $scope.reminderId,
                            receiver_id : $scope.receiverId
                            }
                            var url = server_url() + "completion_date_count";
                           
                            //alert(url);
                            $http.post(url, JSON.stringify(formData))
                            .then(function (response) {
                           
                          
                            $scope.completion_date_cnt = response.data[0].cnt;
                            //   console.log(response.data);
                            }, function (response) {
                            // console.log(response);
                            });


                        }    

   

});
app.config(function ($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl: "home.html",
            reloadOnSearch: false
        })
        .when("/home", {
            templateUrl: "home.html",
            reloadOnSearch: false
        })

        .when("/login", {
            templateUrl: "login.html",
            reloadOnSearch: false
        })
         .when("/registration", {
           templateUrl: "registration.html",
           reloadOnSearch: false
         })

        .when("/profile_update", {
            templateUrl: "profile_update.html",
            reloadOnSearch: false
        })
        .when("/add_receiver", {
            templateUrl: "add_receiver.html",
            reloadOnSearch: false
        })
        .when("/update_receiver/:Receiver_ID", {
            templateUrl: "update_receiver.html",
            reloadOnSearch: false
        })
        .when("/add_reminder", {
            templateUrl: "add_reminder.html",
            reloadOnSearch: false
        })
        .when("/update_reminder/:Reminder_ID", {
            templateUrl: "update_reminder.html",
            reloadOnSearch: false
        })
        .when("/assign_reminderToreceiver/:receiver_id", {
            templateUrl: "assign_reminderToreceiver_part_1.html",
            reloadOnSearch: false
        })
        .when("/assign_reminderToreceiver_part_2/:Reminder_ID/:receiver_id", {
            templateUrl: "assign_reminderToreceiver_part_2.html",
            reloadOnSearch: false
        })
        .when("/assign_receiverToreminder/:reminder_id", {
            templateUrl: "assign_receiverToreminder_part_1.html",
            reloadOnSearch: false
        })
        .when("/assign_receiverToreminder_part_2/:reminder_id/:Receiver_ID", {
            templateUrl: "assign_receiverToreminder_part_2.html",
            reloadOnSearch: false
        })
        .when("/reminder_management", {
            templateUrl: "reminder_management.html",
            reloadOnSearch: false
        })
         .when("/send_reminder", {
                    templateUrl: "send_reminder.html",
                    reloadOnSearch: false
                })
        .when("/receiver_management", {
            templateUrl: "receiver_management.html",
            reloadOnSearch: false
        })
        .when("/manage_completion_date/:reminder_id", {
            templateUrl: "manage_completion_date.html",
            reloadOnSearch: false
        })
        .when("/update_completion_date/:recvr_reminder_id", {
            templateUrl: "update_completion_date.html",
            reloadOnSearch: false
        })
        .when("/inactive_receivers", {
            templateUrl: "inactive_receivers.html",
            reloadOnSearch: false
        })
        .when("/view_receiver/:Receiver_ID", {
            templateUrl: "view_receiver.html",
            reloadOnSearch: false
        })
        .when("/feedback", {
            templateUrl: "feedback.html",
            reloadOnSearch: false
        })
         .when("/completion_reminder_list", {
            templateUrl: "completion_reminder_list.html",
            reloadOnSearch: false
        })
          .when("/user_dashboard", {
            templateUrl: "user_dashboard.html",
            reloadOnSearch: false
        })
		.when("/receiver_dashboard", {
            templateUrl: "receiver_dashboard.html",
            reloadOnSearch: false
        })
        .when("/dashboard_receiver_list", {
            templateUrl: "dashboard_receiver_list.html",
            reloadOnSearch: false
        })

        .when("/sms_success_list", {
                templateUrl: "sms_success_list.html",
                reloadOnSearch: false
         })
         .when("/completion_receiver_list", {
                templateUrl: "completion_receiver_list.html",
                reloadOnSearch: false
         })
          .when("/manage_completion_receiverwise/:receiver_id", {
                templateUrl: "manage_completion_receiverwise.html",
                reloadOnSearch: false
         })
          .when("/dashboard_reminder_list", {
                templateUrl: "dashboard_reminder_list.html",
                reloadOnSearch: false
         })
          .when("/from_to_date_reminder", {
                templateUrl: "from_to_date_reminder.html",
                reloadOnSearch: false
         })
          .when("/from_to_date_receiver", {
                templateUrl: "from_to_date_receiver.html",
                reloadOnSearch: false
         })
          .when("/password_reset", {
                templateUrl: "password_reset.html",
                reloadOnSearch: false
         })
         .when("/contactus", {
                         templateUrl: "contactus.html",
                         reloadOnSearch: false
          })
          .when("/membership",{
                         templateUrl: "membership.html",
                         reloadOnSearch: false
          })



        

});  