
var db = null;
function connections(companyprofile_id) {
	db = window.sqlitePlugin.openDatabase({
		name: 'reminder_db.db',
		location: 'default',
	});

//alert(companyprofile_id);
  db.executeSql("DROP TABLE IF EXISTS reminder_schedule");
 db.executeSql('CREATE TABLE IF NOT EXISTS "reminder_schedule" ("idreminder_schedule_sms" INTEGER, "idcompany_profile" INTEGER, "receiver_name" TEXT, "reminder_name" TEXT, "reminder_text" TEXT, "due_date" DATE, "reminder_date" DATE, "completion_date" DATE, "name" TEXT, "mobile_number" INTEGER, "idreminder" INTEGER, "sms_status" TEXT, "idreceiver_contacts" INTEGER)');

        var params = { company_profile_id: companyprofile_id};
         // HTTP request ////////////////////
                    $.ajax({
                    	url: 'https://mobileapp.thestartupsupport.com/ca-app/sync_sms_schedule',
                    	type: 'POST',
                    	data: JSON.stringify(params),
                    	dataType: 'json',
                    	success: function (res) {

                    		if (res.length != 0) {
                                // ////// Insert transaction /////////////
                                db.transaction(function (tx) {
                                	$.each(res, function (i, item) {
                                       // console.log('item: ' + JSON.stringify(item));

                                       tx.executeSql("INSERT INTO reminder_schedule values (?,?,?,?,?,?,?,?,?,?,?,?,?)", [item.idreminder_schedule_sms, item.idcompany_profile, item.receiver_name, item.reminder_name, item.reminder_text, item.due_date, item.reminder_date, item.completion_date, item.name, item.mobile_number, item.idreminder, item.sms_status, item.idreceiver_contacts]);
                                   });

                                }, function (e) {
                                	//console.log('Transaction error: ' + e.message);
                                	//alert('Transaction error: ' + e.message);
                                }, function () {
                                	db.executeSql('SELECT * FROM reminder_schedule', [], function (res) {
                                		for (var i = 0; i < res.rows.length; i++) {
                                			row = res.rows.item(i);

                                		}

                                		//console.log('Check SELECT result: ' + JSON.stringify(res.rows.item(0)));
                                       // alert('Transaction finished, check record count: ' + JSON.stringify(res.rows.item(0)));
                                        //  send_data(db);
                                    });
                                });

                            } //end if
                        },
                        error: function (e) {
                        	//console.log('Got ERROR: ' + JSON.stringify(e));
                        	//alert('Got ERROR: ' + JSON.stringify(e));
                        }
                    });




};


// Select all checkboxes
function selectall(source) {
   // alert("test");
   checkboxes = document.getElementsByName('idreminder_schedule_sms[]');
   for(var i=0, n=checkboxes.length;i<n;i++) {
   	checkboxes[i].checked = source.checked;
   }
}


function show_reminders_list(company_profile_id,smsStatus = null) {

	db = window.sqlitePlugin.openDatabase({
		name: 'reminder_db.db',
		location: 'default',
	});




    // Retrieve reminder list
    var data = "";
    var dbarr = [];
     var accordion_body = '';
    /* var sql = "SELECT *,'pending' as status FROM reminder_schedule where idcompany_profile = "+company_profile_id+" and sms_status is null union SELECT *,'success' as status FROM reminder_schedule where idcompany_profile = "+company_profile_id+" and sms_status = 'success' order by receiver_name,due_date asc";*/
    if(smsStatus == null)
    {

        var sql = "SELECT idreminder_schedule_sms,receiver_name,reminder_name,name,mobile_number,sms_status FROM reminder_schedule where idcompany_profile = "+company_profile_id+" and sms_status is null order by receiver_name,due_date, reminder_name asc";
       // console.log("Pending "+ sql);


    }else{
        var sql = "SELECT idreminder_schedule_sms,receiver_name,reminder_name,name,mobile_number,sms_status FROM reminder_schedule where idcompany_profile = "+company_profile_id+" and sms_status = 'success' order by receiver_name,due_date, reminder_name asc";

      //  console.log("Success "+ sql);

    }


    db.executeSql(sql, [],
    	function (res) {
       // console.log("step 1");
    var pageUrl = window.location.hash;
      if(res.rows.length == 0 ){

      	data ='<h3>No Reminders For Today!!!!</h3>';

        data += '<a href="#!/sms_success_list" class="float-right btn btn-outline-primary">SMS Sent List</a><br>';


      }else{

            //create an array
            for(k=0; k < res.rows.length ; k++)
            {
                dbarr.push(res.rows.item(k));
            }

    dbarr.push('');
 //console.log("step 2" + JSON.stringify(res));


      	if(pageUrl == '#!/send_reminder')
      	{   data = '<button id="btn_sms" type="button" class="login100-form-btn" onclick="send_sms()">Send SMS  <i class="material-icons">send</i></button>';
      	    data += '<a href="#!/sms_success_list" class="float-right btn btn-outline-primary">SMS Sent List</a><br>';
      	    data += '<span><input type="checkbox" name="selectall" onclick="selectall(this);" checked></span>';
                  	data += '<span>Select All</span>';
      	}
      	else if(pageUrl == '#!/sms_success_list'){
      	    data += '<div class="col-12"><a href="#!/send_reminder" class="btn btn-outline-primary">Pending SMS List</a></div><br>';
      	}


          //  data += '<th>Mobile Number</th>';
           // data += '<th>SMS Status</th>';
          // data += '</div>';




           var receiver_name = '';
           var next_receiver_name = '';
           data += '<div id="accordion">';

           var next_cnt = 0;

           var last_cnt = dbarr.length - 1;
           var j=0;
           data += '<div class="card">';
        //console.log("step 3");
       // dbarr.push('');
           for (i = 0; i < dbarr.length; i++) {
               // dbarr.push(res.rows.item(i));
                    console.log("Results array "+ JSON.stringify(dbarr[i]));
                  var sms_status = dbarr[i].sms_status == null ? "Pending" :  dbarr[i].sms_status;
                  //var checkbox_status = res.rows.item(i).status;
                  if (next_cnt < dbarr.length) {
                  	next_cnt = next_cnt + 1;
                  }

                  if (next_cnt == dbarr.length) {
                  	next_cnt = next_cnt - 1;
                  }

                  if ( i == 0)

                  {

                  	accordion_body += '<div><strong>' + dbarr[i].reminder_name + '</strong></div>';

                  }

                    if(sms_status == 'success')
                    {
                     accordion_body += '<div>' + dbarr[i].name + ' ( ' + dbarr[i].mobile_number + ') <span class="float-right badge badge-success">'+sms_status+'</span></div>';
                    }
                    else{
                    accordion_body += '<div> <input  type="checkbox"  name="idreminder_schedule_sms[]" value='+ dbarr[i].idreminder_schedule_sms +' checked> ' + dbarr[i].name + ' ( ' + dbarr[i].mobile_number + ') <span class="float-right badge badge-success">'+sms_status+'</span></div>';
                    }






                  if ((dbarr[next_cnt].receiver_name != dbarr[i].receiver_name ) && dbarr[i].receiver_name != 'undefined')
                  {

                  	data += ' <div class="card-header" id="headingOne' + i + '">';
                  	data += '   <h5 class="mb-0">';
                  	data += '    <span class="collapsed" data-toggle="collapse" data-target="#collapseOne' + i + '" aria-expanded="false" aria-controls="collapseOne">';
		            // data+= arr[i].receiver_name + ' - '+next_cnt + ' - '+arr.length + '-' + arr[next_cnt].receiver_name ;

		            data += dbarr[i].receiver_name+"<span class='float-right'> <i class='material-icons'>expand_more</i> </span>";

		            data += '      </span>';
		            data += '    </h5>';
		            data += '  </div>'; // end card header

		            data += '  <div id="collapseOne' + i + '" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">';
		            data += '     <div class="card-body">';


		            data += accordion_body;



		            data += '    </div>'; //end card body
		            data += '   </div>'; // end collapseOne

		            accordion_body = '';

		        }



		        if (dbarr[next_cnt].reminder_name != dbarr[i].reminder_name || dbarr[next_cnt].receiver_name != dbarr[i].receiver_name )

		        {

		        	    j++;
                     accordion_body += '<div class="divider"></div>';
                     accordion_body += '<div><strong>' + dbarr[next_cnt].reminder_name + '</strong></div>';


		        }




          }


				data += ' </div>'; //end card
				data += '</div>'; //accordion
      }
      document.getElementById('smsdata').innerHTML = data;

  }, function (error) {
  	console.log('SELECT SQL statement ERROR: ' + error.message);
  });
}

function send_sms() {

	document.getElementById("btn_sms").disabled = true;

	var checkboxes = document.getElementsByName('idreminder_schedule_sms[]');

	var vals = '';

    var id_arr = [];
    for (var i=0, n=checkboxes.length;i<n;i++) {
        if (checkboxes[i].checked) {
            vals += ","+checkboxes[i].value;
            id_arr.push(checkboxes[i].value);
        }
    }

    /*if(id_arr.length != 0)
    {


    }
    else{
    alert("Please select atleast one Recipient");
    document.getElementById("btn_sms").disabled = false;
    return false;
}*/
  // console.log("selected nos " + id_arr.toString());


//alert("cbmzxc");
db.transaction(function (transaction) {
	var executeQuery = "SELECT * FROM reminder_schedule where idreminder_schedule_sms in (" + id_arr.toString() + ")";
	console.log(executeQuery);
	transaction.executeSql(executeQuery, [],
            //On Success
            //console.log("hellooo");
            function (tx, res) {
 //alert(res.rows.length);
 for (i = 0; i < res.rows.length; i++) {


 	var number = res.rows.item(i).mobile_number;
 	var message = res.rows.item(i).reminder_text ;
 	var idreminder_schedule_sms = res.rows.item(i).idreminder_schedule_sms;

 	console.log("number=" + number + ", message= " + message + "ID = " + idreminder_schedule_sms);

                    //CONFIGURATION
                    var options = {
                        replaceLineBreaks: false, // true to replace \n by a new line, false by default
                        android: {
                            //intent: 'INTENT'  // send SMS with the native android SMS messaging
                            intent: '' // send SMS without opening any other app
                        }
                    };
//alert("m here");
sms.send(number, message, options, success, error);

                  //  console.log("Success callback " + success);
                  db.executeSql('UPDATE reminder_schedule SET sms_status = "success" WHERE                                                                    idreminder_schedule_sms = ?', [idreminder_schedule_sms],
                  	function (res) { console.log("Success Updated successfully" + idreminder_schedule_sms); }
                  	, function (e) {
                  		console.log("Success Update Failed");

                  	});

                        // Update SMS status to server
                        send_data(db);

                        var success = function () {

                        	console.log('Entered success fn ');


                        };
                        var error = function () {
                               // console.log('Entered error fn ');
                               alert("SMS sending failed");
                           };


                       }

                       alert("Your request for sending SMS has been processed");
                       window.location = '#!/';

                   },
            //On Error
            function (error) { console.log("Could not Send SMS"); });
});
}



// Update SMS status to server

function send_data(db){
	var arr = [];

	db.executeSql('SELECT idreminder_schedule_sms,sms_status  FROM reminder_schedule', [], function(res) {
		for (var i=0; i < res.rows.length; i++){
			row = res.rows.item(i);
			arr.push(row);
			//console.log("row senddata is " + JSON.stringify(row));

		}
		console.log('SELECT SQL Array: ' + JSON.stringify(arr));

                       // http request to send the data to server
                       $.ajax({
                       	url: 'https://mobileapp.thestartupsupport.com/ca-app/sms_status_data',
                       	type:'POST',
                       	data: JSON.stringify(arr),
                       	dataType: 'json',
                       	success: function(res) {
                       		//console.log(JSON.stringify(res));

                       	},
                       	error: function(e) {
                       		//console.log('Got ERROR: ' + JSON.stringify(e));
                       		//alert('Got ERROR: ' + JSON.stringify(e));
                       	}
                       });


                   }, function(error) {
                   //	console.log('SELECT SQL statement ERROR: ' + error.message);
                   });


};
